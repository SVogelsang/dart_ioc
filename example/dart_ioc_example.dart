import 'package:dart_ioc/dart_ioc.dart';

abstract class A {
}

class B extends A {
}

main() {

  var containerWithSingletons = Container()
  // register singleton instance
    .registerSingleton('Hello World')
  // register singleton instantiated on first resolve using a factory method.
    .registerSingletonLazy<Stream>(() => Stream.value(100))
  // register singleton instantiated on first resolve using a factory method.
    .registerSingletonLazy<B>();

  String helloWorld = containerWithSingletons.resolve<String>();  
  print(helloWorld); // prints out 'Hello World'

  var containerWithTypes = Container()
  // register type.
    .registerType<B>()
  // register type with factory method.
    .registerType(() => 'Hello World');

  helloWorld = containerWithTypes.resolve<String>();  
  print(helloWorld); // prints out 'Hello World'

  var containerWithBaseTypes = Container()
  // register type as base type.
    .register<A,B>();

  B b = containerWithBaseTypes.resolve<A>();  
  print(b); // prints out 'Instance of 'B''

  // merge containers
  Container container1 = Container().registerType(() => 'Hello World');
  Container container2 = Container().registerType(() => 100);

  print(container1.merge(container2).resolve<int>()); // prints out 100
}
