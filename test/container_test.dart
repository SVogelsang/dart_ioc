import 'package:dart_ioc/dart_ioc.dart';
import 'package:dart_ioc/src/registration_exception.dart';
import 'package:dart_ioc/src/resolve_exception.dart';
import 'package:test/test.dart';

abstract class _TAbstract {
}

class _T extends _TAbstract {
}

class _WithPositionalParameters {

  String _name;
  _T _otherClass;
  String get name => _name;
  _T get otherClass => _otherClass;

  _WithPositionalParameters(String name, _T otherClass) {
    _name = name;
    _otherClass = otherClass;
  }
}

class _WithNamedParameters {

  String name;
  _T otherClass;
  
  _WithNamedParameters({this.name, this.otherClass});
}

class _WithBothParameters {

  String _name;
  int number;
  _T otherClass;
  String get name => _name;

  _WithBothParameters(String name, {this.otherClass, this.number}) {
    _name = name;
  }
}

void main() {
  group('Container', () {

    test('Should have registered the container itself', () {
      final sut = Container();
      expect(sut.resolve<Container>(), equals(sut));
    });

    test('Should have registered the container itself as singleton', () {
      final sut = Container();
      expect(sut.resolve<Container>(), equals(sut.resolve<Container>()));
    });

    group('Registration', () {

      group('Singleton', () {

        test('Should throw if registered type is dynamic', () {

          expect(() => Container().registerSingleton<dynamic>(Stream.value(100)), throwsA(TypeMatcher<RegistrationException>()));
        });

        test('Should register a specified instance', () {

          final sut = Container();
          sut.registerSingleton('Test');
          expect(sut.resolve<String>(), equals('Test'));
        });

        test('Should register a specified instance as abstract type', () {

          final sut = Container();
          final reg = Stream.value('test');
          sut.registerSingleton<Stream>(reg);
          expect(sut.resolve<Stream>(), equals(reg));
        });

        test('Should throw if registered instance is null', () {

          final sut = Container();
          expect(() => sut.registerSingleton<Stream>(null), throwsA(TypeMatcher<RegistrationException>()));
        });

        test('Should register concrete type if generic type is not specified', () {

          final sut = Container().registerSingleton(Stream.value(100));
          expect(sut.resolve<Stream<int>>(), isNotNull);
        });
      });

      group('Lazy singleton', () {

        test('Should throw if registered type is dynamic', () {

          expect(() => Container().registerSingletonLazy<dynamic>(), throwsA(TypeMatcher<RegistrationException>()));
        });

        test('Should throw if registered type is abstract and there is not factory method specified', () {

          expect(() => Container().registerSingletonLazy<Stream>(), throwsA(TypeMatcher<RegistrationException>()));
        });

        test('Should not throw if registered type is abstract and there is factory method specified', () {

          expect(Container().registerSingletonLazy<Stream>(() => Stream.value(100)), isA<Container>());
        });

        test('Should register normal resolve procedure', () {

          final sut = Container().registerSingletonLazy<_T>();
          expect(sut.resolve<_T>(), isNotNull);
        });

        test('Should return same instance when resolving twice', () {

          final sut = Container().registerSingletonLazy<_T>();
          expect(sut.resolve<_T>(), equals(sut.resolve<_T>()));
        });

        test('Should register a factory method for creating the singleton instance', () {

          var called = false;
          var factoryMethod = () {
            called = true;
            return _T();
          };

          final sut = Container().registerSingletonLazy(factoryMethod);
          expect(sut.resolve<_T>(), isA<_T>());
          expect(called, isTrue);
        });

        test('Should return same instance when resolving twice created by factory method', () {

          final sut = Container().registerSingletonLazy(() => _T());
          expect(sut.resolve<_T>(), equals(sut.resolve<_T>()));
        });
      });
    
      group('Types', () {

        test('Should register a specified type', () {

          final sut = Container().registerType<_T>();
          expect(sut.resolve<_T>(), isA<_T>());
        });

        test('Should register a specified type with factory method', () {

          var called = false;
          var factoryMethod = () {
            called = true;
            return _T();
          };
          final sut = Container().registerType<_TAbstract>(factoryMethod);
          expect(sut.resolve<_TAbstract>(), isA<_T>());
          expect(called, isTrue);
        });

        test('Should throw if registered type is abstract', () {

          expect(() => Container().registerType<Stream>(), throwsA(TypeMatcher<RegistrationException>()));
        });
      });

      group('Abstract Types', () {

        test('Should register a specified type as abstract base type', () {

          final sut = Container().register<_TAbstract, _T>();
          expect(sut.resolve<_TAbstract>(), isA<_T>());
        });

        test('Should throw if registered type is abstract', () {

          expect(() => Container().register<Stream, Stream>(), throwsA(TypeMatcher<RegistrationException>()));
        });

        test('Should throw if registered type is dynamic', () {

          expect(() => Container().register<dynamic, _T>(), throwsA(TypeMatcher<RegistrationException>()));
        });

        test('Should throw if instance type is abstract', () {

          expect(() => Container().register<_TAbstract, _TAbstract>(), throwsA(TypeMatcher<RegistrationException>()));
        });
      });
    });

    group('Resolve instances', () {

      test('Should throw if type is not registered', () {

        final sut = Container();
        expect(() => sut.resolve<String>(), throwsA(TypeMatcher<ResolveException>()));
      });

      test('Should resolve registered type', () {

        final sut = Container().registerType<String>(() => 'Hello World');
        expect(sut.resolve<String>(), equals('Hello World'));
      });

      test('Should resolve constructor with positional parameters', () {

        final sut = Container()
          .registerType<String>(() => 'Hello World')
          .registerType<_T>()
          .registerType<_WithPositionalParameters>();

        expect(sut.resolve<_WithPositionalParameters>(), isA<_WithPositionalParameters>());
        expect(sut.resolve<_WithPositionalParameters>().name, equals('Hello World'));
        expect(sut.resolve<_WithPositionalParameters>().otherClass, isA<_T>());
      });

      test('Should resolve constructor with named parameters', () {

        final sut = Container()
          .registerType<String>(() => 'Hello World')
          .registerType<_T>()
          .registerType<_WithNamedParameters>();

        expect(sut.resolve<_WithNamedParameters>(), isA<_WithNamedParameters>());
        expect(sut.resolve<_WithNamedParameters>().name, equals('Hello World'));
        expect(sut.resolve<_WithNamedParameters>().otherClass, isA<_T>());
      });

      test('Should resolve constructor with named and positional parameters', () {

        final sut = Container()
          .registerType<String>(() => 'Hello World')
          .registerType<int>(() => 100)
          .registerType<_T>()
          .registerType<_WithBothParameters>();

        expect(sut.resolve<_WithBothParameters>(), isA<_WithBothParameters>());
        expect(sut.resolve<_WithBothParameters>().name, equals('Hello World'));
        expect(sut.resolve<_WithBothParameters>().number, 100);
        expect(sut.resolve<_WithBothParameters>().otherClass, isA<_T>());
      });
    });

    group('Merge Containers', () {

      test('Should merge types if there are no duplicate registrations', () {

        final sut = Container().registerType<_T>();
        final merged = sut.merge(Container().registerType<String>(() => 'Hello World'));
        expect(merged.resolve<_T>(), isA<_T>());
        expect(merged.resolve<String>(), equals('Hello World'));
      });
 
      test('Should merge type with constructor resolved from different containers', () {

        final sut = Container().registerType<_T>();
        final other = Container()
          .registerType<String>(() => 'Hello World')
          .registerType<_WithPositionalParameters>();
        sut.merge(other);
        expect(sut.resolve<_WithPositionalParameters>(), isA<_WithPositionalParameters>());
      });
 
      test('Should throw if there are types registered in both containers', () {

        final sut = Container()
          .registerType<String>(() => 'Hello World');
        final other = Container()
          .registerType<String>(() => 'Hello other World');
        expect(() => sut.merge(other), throwsA(TypeMatcher<RegistrationException>()));
      });
    });

    group('To String', () {

      test('Should write 1 registration if container is empty (container itself is registered)', () {

        expect(Container().toString(), equals('1 Registrations: [Container]'));
      });

      test('Should write registration and registered types if container is not empty', () {

        final sut = Container()
        .registerType(() => 'Hello World')
        .register<_TAbstract, _T>()
        .registerSingleton(100)
        .registerSingletonLazy(() => 100.0);

        expect(sut.toString(), equals('5 Registrations: [Container, String, _TAbstract, int, double]'));
      });
    });
  });
}
