import 'package:test/test.dart';
import 'package:dart_ioc/src/type_validator.dart';
import 'package:dart_ioc/src/registration_exception.dart';

void main() {
  group('TypeValidator', () {

    test('Should succeed if type is not dynamic', () {

      expect(TypeValidator.validate<String>().isNotDynamic(), isA<TypeValidator>());
    });

    test('Should throw if type is dynamic', () {

      expect(() => TypeValidator.validate<dynamic>().isNotDynamic(), throwsA(TypeMatcher<RegistrationException>()));
    });

    test('Should throw if type is abstract', () {

      expect(() => TypeValidator.validate<Stream>().isNotAbstract(), throwsA(TypeMatcher<RegistrationException>()));
    });

    test('Should chain validators using and', () {

      expect(() => TypeValidator.validate<Stream>().isNotDynamic().and().isNotAbstract(), throwsA(TypeMatcher<RegistrationException>()));
    });

    test('Should chain validators for diefferent types', () {

      expect(() => TypeValidator.validate<String>().isNotDynamic().and().type<Stream>().isNotAbstract(), throwsA(TypeMatcher<RegistrationException>()));
    });
  });
}
