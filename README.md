[![pipeline status](https://gitlab.com/SVogelsang/dart_ioc/badges/master/pipeline.svg)](https://gitlab.com/SVogelsang/dart_ioc/commits/master)
[![coverage report](https://gitlab.com/SVogelsang/dart_ioc/badges/master/coverage.svg)](https://gitlab.com/SVogelsang/dart_ioc/commits/master)

A simple IoC Container for [Dart](https://dart.dev/).

## Usage

A simple usage example:

```dart
import 'package:dart_ioc/dart_ioc.dart';

main() {
  var container = new Container()
    .register<A>
    .registerSingleton(Stream.value(100));

  var stream = container.resolve<Stream>()  
}
```

See also [examples](./example/dart_ioc_example.dart) for detailed examples.

## Features and bugs

Please file feature requests and bugs at the [issue tracker][tracker].

[tracker]: https://gitlab.com/SVogelsang/dart_ioc/-/boards/1411785
