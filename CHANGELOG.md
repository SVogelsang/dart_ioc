# Change Log for dart_ioc
An IoC container implementation for dart and flutter with support for types, singletons, factory methods and merging of containers. 

## v1.0.1 - 2020-01-12

### Docs
* maintenance [265425e]

## v1.0.0 - 2019-11-16

### Feature
* Initial Release [38974a5]


This CHANGELOG.md was generated with [**Changelog for Dart**](https://pub.dartlang.org/packages/changelog)
