import 'dart:mirrors';
import 'package:dart_ioc/src/registration_exception.dart';

class TypeValidator {
  Type _type;

  TypeValidator(Type type) {
    _type = type;
  }

  static TypeValidator validate<T>() {
    return TypeValidator(T);
  }

  TypeValidator and() {
    return this;
  }

  TypeValidator type<T>() {
    return TypeValidator(T);
  }

  TypeValidator isNotAbstract() {
    if (reflectClass(_type).isAbstract) {
      throw RegistrationException(
          'Type \'$_type\' is abstract and cannot be instantiated');
    }

    return this;
  }

  TypeValidator isNotDynamic() {
    if (_type == dynamic) {
      throw RegistrationException(
          'Type \'$_type\' is dyamic and cannot be registered');
    }

    return this;
  }
}
