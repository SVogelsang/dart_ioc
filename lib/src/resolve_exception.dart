class ResolveException implements Exception {
  String message;
  @pragma('vm:entry-point')
  ResolveException(this.message);
}
