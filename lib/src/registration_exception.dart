class RegistrationException implements Exception {
  String message;
  @pragma('vm:entry-point')
  RegistrationException(this.message);
}
