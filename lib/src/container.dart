import 'dart:mirrors';

import 'package:dart_ioc/src/registration_exception.dart';
import 'package:dart_ioc/src/resolve_exception.dart';
import 'package:dart_ioc/src/type_validator.dart';

/// The IoC Container.
class Container  {

  final Map<Type, dynamic Function([Map<Type,Function> registrations])> _registrations = {};

  /// Creates an instance of the IoC Container and registeres the container instance itself as a singleton.
  Container() {

    registerSingleton(this);
  }

  /// Register an instance as singleton.
  /// 
  /// [instance] is the instance to be registered. [TReg] is optional and must be a base type of the [instance] to be registered. 
  /// If [TReg] is not specified the singleton can be resolved by it's type; otherwise it must be resolved by [TReg]. 
  /// 
  /// [instance] The singleton instance to be registered.
  /// Returns the container instance itself to allow chaining of registrations.
  /// Throws a [RegistrationException] if [instance] is [:null:].
  /// 
  /// ```dart
  /// Container container = Container()
  ///   .registerSingleton<B>();
  /// print(container.resolve<B>()); // prints out 'Instance of 'B''
  /// ```
  Container registerSingleton<TReg>(TReg instance) {

    TypeValidator.validate<TReg>().isNotDynamic();

    if(instance == null) {

      throw RegistrationException('Registered instance not defined');
    }

    _registrations[TReg] = ([_]) => instance;
    return this;
  }

  /// Register a [TReg] as singleton. The instance is created when it is resolved the first time.
  /// 
  /// [factoryMethod] is an optional factory method for creating the instance. If not specified the instance is created through normal [resolve] procedure.
  /// Returns the container instance itself to allow chaining of registrations.
  /// 
  /// ```dart
  /// Container container = Container()
  ///   .registerSingletonLazy<B>();
  ///   .registerSingletonLazy<A>(() => B());
  /// print(container.resolve<B>()); // prints out 'Instance of 'B''
  /// print(container.resolve<A>()); // prints out 'Instance of 'B''
  /// ```
  Container registerSingletonLazy<TReg>([TReg Function() factoryMethod]) {

    TypeValidator.validate<TReg>().isNotDynamic();

    if(factoryMethod == null) {

      TypeValidator.validate<TReg>().isNotAbstract();
    }

    _registrations[TReg] = ([registrations]) {

      var instance = factoryMethod != null ? factoryMethod() : _instantiate<TReg>(registrations);
      _registrations[TReg] = ([_]) => instance;
      return instance;
    };

    return this;
  }

  /// Register a [TInstance] as [TReg]. The instance is created when the [TReg] is resolved.
  /// 
  /// Returns the container instance itself to allow chaining of registrations.
  /// 
  /// ```dart
  /// Container container = Container()
  ///   .register<A, B>();
  /// print(container.resolve<A>()); // prints out 'Instance of 'B''
  /// ```
  Container register<TReg, TInstance extends TReg>() {

    TypeValidator.validate<TReg>().isNotDynamic()
      .and().type<TInstance>().isNotAbstract();

    _registrations[TReg] = ([registrations]) => _instantiate<TInstance>(registrations);
    return this;
  }

  /// Register a [TReg]. The instance is created when the [TReg] is resolved.
  /// 
  /// [factoryMethod] is an optional factory method for creating the instance. If not specified the instance is created through normal [resolve] procedure.
  /// Returns the container instance itself to allow chaining of registrations.
  /// 
  /// ```dart
  /// Container container = Container()
  ///   .registerType<Stream>(() => Stream.value(100));
  ///   .registerType<A>();
  /// ```
  Container registerType<TReg>([TReg Function() factoryMethod]) {

    TypeValidator.validate<TReg>().isNotDynamic();

    if(factoryMethod == null) {

      TypeValidator.validate<TReg>().isNotAbstract();
    }

    _registrations[TReg] = ([registrations]) => factoryMethod != null ? factoryMethod() : _instantiate<TReg>(registrations);
    return this;
  }

  /// Resolves a [TReg] instance and resolves all it's dependencies.
  /// 
  /// Returns the resolved instance.
  /// 
  /// ```dart
  /// Container container = Container()
  ///   .registerType<Stream>(() => Stream.value(100));
  /// 
  /// Stream stream = container.resolve<Stream>();
  /// ```
  TReg resolve<TReg>() {

    if(_registrations[TReg] == null) {

      throw ResolveException('Type $TReg is not registered');
    }

    return _registrations[TReg](_registrations);
  }

  /// Merge this [Container] with another container.
  /// 
  /// [other] The container to be merged into this container.
  /// Returns the container instance itself to allow chaining.
  /// Throws [ResolveException] if this container contains types already registered in the [other] container.
  /// 
  /// ```dart
  /// Container container = Container().registerType<Stream>(() => Stream.value(100));
  /// Container otherContainer = Container().registerType(() => 100);
  /// int number = container.merge(otherContainer).resolve<int>();
  /// ```
  Container merge(Container other) {

    final conflictType = other._registrations.keys.singleWhere((treg) => treg != Container && _registrations.keys.contains(treg), orElse: () => null);
    if(conflictType != null) {

      throw RegistrationException('Cannot merge with other container because type \'$conflictType}\' is already registered');
    }
    
    other._registrations.forEach((treg, resolver) {

      if(treg != Container) {

        _registrations[treg] = resolver;
      }
    });

    return this;
  }

  @override
  String toString() {

    var str = '${_registrations.keys.length} Registrations';

    if(_registrations.keys.isNotEmpty) {

      var registrations = _registrations.keys.join(', ');
      str += ': [$registrations]';
    }
    return str;
  }

  static TCreate _instantiate<TCreate>(Map<Type, Function> registrations) {

    var mirror = reflectClass(TCreate);
    var constructors = List.from(mirror.declarations.values.where((declare) => declare is MethodMirror && declare.isConstructor));

    if(constructors.isEmpty) {

      return mirror.newInstance(Symbol(''), []).reflectee;
    }

    MethodMirror resolveableConstructor = constructors.singleWhere((ctor) => (ctor as MethodMirror).parameters.every((p) => registrations[p.type.reflectedType] != null || p.hasDefaultValue), orElse: () => null);

    if(resolveableConstructor == null) {

      throw ResolveException('Cannot find any resolveable constructor for type $TCreate');
    }

    final resolvedParameters = List.from(resolveableConstructor.parameters
      .map((p) => {'parameter': p, 'value': registrations[p.type.reflectedType] == null && p.hasDefaultValue ? p.defaultValue.reflectee :  registrations[p.type.reflectedType](registrations)}))
      .toList();
    final namedParameters  = <Symbol,dynamic>{};
    resolvedParameters.where((p) => (p['parameter'] as ParameterMirror).isNamed).forEach((n) => namedParameters[n['parameter'].simpleName] = n['value']);
    final positionalParameters = resolvedParameters.where((p) => !(p['parameter'] as ParameterMirror).isNamed).map((p) => p['value']).toList();
    return mirror.newInstance(resolveableConstructor.constructorName, positionalParameters, namedParameters).reflectee;
  }
}
